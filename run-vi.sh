# add parsed references to prepare for the volume importer run
cd ../fna-data/fna-data-curation/scripts/done/python/add/parsed_references
python add_parsed_refs.py coarse /Users/jocelynpender/fna/fna-data/fna-data-curation/supplemental_data/references/**
cd ~/fna/volume-importer

yarn run build # ensure you have the latest up-to-date yarn packages
npm i # or use npm!

# run the volume importer workflow on all volumes
# for dir in ../fna-data/fna-data-curation/coarse_grained_fna_xml/V*; do rm -r ./output/*; do npm start -- "$dir"; mv output/* ../fna-data/fna-smw-ready-xml-pages/volume_importer_xml/coarse_grained_files/$(basename $dir)/; echo "done $dir"; done
for dir in ../fna-data/fna-fine-grained-xml/V*; do rm -r ./output/*; do npm start -- "$dir"; mv output/* ../fna-data/fna-smw-ready-xml-pages/volume_importer_xml/fine_grained_files/$(basename $dir)/; echo "done $dir"; done

# italicize the output from volume importer
cd ../fna-data/fna-smw-ready-xml-pages/scripts
python3 process_all.py
cd ../../../volume-importer

