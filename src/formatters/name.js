import ucFirst from '../util/ucfirst'

const formatters = {
  family: (hierarchy) => ucFirst(`${hierarchy.family}`),
  subfamily: (hierarchy) => ucFirst(`${hierarchy.family}`) + ` subfam. ` + ucFirst(`${hierarchy.subfamily}`),
  tribe: (hierarchy) => ucFirst(`${hierarchy.family}`) + ` tribe ` + ucFirst(`${hierarchy.tribe}`),
  subtribe: (hierarchy) => ucFirst(`${hierarchy.family}`) + ` (tribe ` + ucFirst(`${hierarchy.tribe}`) + `) subtribe ` + ucFirst(`${hierarchy.subtribe}`),
  genus: (hierarchy) => ucFirst(`${hierarchy.genus}`),
  subgenus: (hierarchy) => ucFirst(`${hierarchy.genus}`) + ` subg. ` + ucFirst(`${hierarchy.subgenus}`),
  section: (hierarchy) => ucFirst(`${hierarchy.genus}`) + ` sect. ` + ucFirst(`${hierarchy.section}`),
  subsection: (hierarchy) => ucFirst(`${hierarchy.genus}`) + ` subsect. ` + ucFirst(`${hierarchy.subsection}`),
  series: (hierarchy) => ucFirst(`${hierarchy.genus}`) + ` (sect. ` + ucFirst(`${hierarchy.section}`) + `)` + ` ser. ` + ucFirst(`${hierarchy.series}`),
  species: (hierarchy) => ucFirst(`${hierarchy.genus} ${hierarchy.species}`),
  subspecies: (hierarchy) => ucFirst(`${hierarchy.genus} ${hierarchy.species} subsp. ${hierarchy.subspecies}`),
  variety: (hierarchy) => ucFirst(`${hierarchy.genus} ${hierarchy.species} var. ${hierarchy.variety}`)
}
const defaultFormatter = (rank, h) => ucFirst(h[rank])
export default (id, rank) => {
  if (!rank) {
    rank = id.rank
  }
  return formatters[rank]
    ? formatters[rank](id.hierarchyMap)
    : defaultFormatter(rank, id.hierarchyMap)

  }
