import fs from 'fs'
import path from 'path'
import { expect } from 'chai'
import {DOMParser} from 'xmldom'
import selectName from '../name'

// output of acceptedId selector
const fixture = {
  rank: 'species',
  publication: {
    title: 'in A. P. de Candolle and A. L. P. P. de Candolle, Prodr.',
    place: '7: 66. 1838'
  },
  hierarchyMap: {
    family: 'asteraceae',
    tribe: 'mutisieae',
    genus: 'acourtia',
    species: 'microcephala'
  }
}

describe('name', function () {
  it('should return a correctly formatted taxon name', function () {
    const name = selectName(fixture)
    expect(name).to.be.a('string')
    expect(name).to.equal('Acourtia microcephala')
  })

  it('should correctly format names for all ranks')
})
