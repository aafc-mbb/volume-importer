import xpath from 'xpath'
import {XMLSerializer} from 'xmldom'

const namespaced = xpath.useNamespaces({"bio": "http://www.github.com/biosemantics"})

const ensureDoc = (doc) => {
  if (!doc) {
    throw new Error("Missing doc")
  }
}

export const select = (path, doc) => {
  try {
    ensureDoc(doc)
    const res = namespaced(path, doc)
    if (!res) {
      throw new Error(`XPath failed for ${path}`)
    }
    return res
  } catch (e) {
    const s = new XMLSerializer()
    console.error(e)
    // console.error(s.serializeToString(doc))
  }
}

export const selectOne = (path, doc) => {
  try {
    ensureDoc(doc)
    const res = select(path, doc)
    if (!res) {
      console.error(`selectOne returned no results for ${path}`)
    }
    return res && res.length ? res[0] : null
  } catch (e) {
    console.log(e)
  }
}

export const selectText = (path, doc) => {
  const res = selectOne(path, doc)
  return res ? res.textContent.replace(/[\t\n\r ]+/g, ' ').trim() : null
}
