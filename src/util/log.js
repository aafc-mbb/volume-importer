const colors = require('colors/safe')

export default function log (message){
  console.log(message);
}
log.errorList = []
log.error = function (message) {
  log(colors.red(message));
  log.errorList.push(message);
}
log.xmlErrorList = []
log.xmlError = function (message, file) {
  console.log(colors.red(message));
  log.xmlErrorList.push([message, file])
}

log.dump = function () {
  if (log.errorList.length) {
    console.log("\n");
    console.log(colors.red.underline('General Errors Encountered During Run'));
    log.errorList.forEach(message => console.log(message));
  }
  if (log.xmlErrorList.length) {
    console.log("\n");
    console.log(colors.red.underline('XML Errors Encountered During Run'));
    log.xmlErrorList.forEach(([message, file]) => console.log(`${file}: ${message}`));
  }
}