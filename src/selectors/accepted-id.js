import {select, selectOne, selectText} from 'util/xpath'
import log from 'util/log'

export default (doc, fileName) => {
  const acceptedId = selectOne('/bio:treatment/taxon_identification[@status="ACCEPTED"]', doc)
  if (acceptedId.length < 1) {
    throw new Error('No accepted ID found')
  }
  const lastTaxonName = selectOne('taxon_name[last()]', acceptedId)

  const publications = select('place_of_publication', acceptedId).map(publication => {
    const title = selectText('./publication_title/text()', publication)
    const place = selectText('./place_in_publication/text()', publication)
    const year = place ? place.match(/[0-9]{4}$/) && place.match(/[0-9]{4}$/)[0] : null;
    const other_info_on_pub = selectText('./other_info_on_pub/text()', publication)
    // console.log(other_info_on_pub)
    return ({title, place, year, other_info_on_pub})
  })

  const hierarchyText = selectText('taxon_hierarchy/text()', acceptedId)

  if (!hierarchyText) {
    log.xmlError('No taxon_hierarchy found', fileName)
  }

  // grab the last item in the hierarchy, then get it's rank
  const hierarchyArr = hierarchyText
    .replace(/;$/, '') // remove any trailing semicolon
    .split(';')
    .map(rankAndName => rankAndName.split(' ').map(v => v.trim()))

  const authority = selectText('./@authority', lastTaxonName)

  const rank = hierarchyArr[hierarchyArr.length - 1][0]

  const hierarchyMap = hierarchyArr.reduce((obj, v) => (obj[v[0]] = v[1]) && obj, {})

  const etymology = selectText('other_info_on_name[@type="etymology"]', acceptedId)

  return {
    rank,
    authority: authority == 'unknown' ? '' : authority, // if the authority is unknown, display nothing
    publications,
    hierarchy: hierarchyArr,
    hierarchyMap,
    hierarchyText: hierarchyText,
    etymology
  }
}
