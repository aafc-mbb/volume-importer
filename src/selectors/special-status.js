import {select, selectOne, selectText} from '../util/xpath'

export default (doc) => {
  const specialStatus = select('/bio:treatment/taxon_identification[@status="ACCEPTED"]/other_info_on_name[@type="special_status"]/text()', doc)
  if (!specialStatus || !specialStatus.length) {
    return false
  }

  return specialStatus.map(
    node => node.data
  )
}
