import { selectText } from '../util/xpath'

export default (doc, isFinegrained) => {
  var text = null
  var value = null
  var re = /(I|i)ntroduced(;|,)\s*/

  if (isFinegrained) {
    text = selectText('(/bio:treatment/description[@type="distribution"])[1]//text', doc)
  } else {
    text = selectText('(bio:treatment/description[@type="distribution"])', doc)
  }

  if (!text || !text.length) {
    return false
  } else {
    return text.match(re) ? true : false
  }
}
