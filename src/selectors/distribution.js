import {select, selectOne, selectText} from '../util/xpath'

export default (doc, isFinegrained) => {
  var text = null
  var values = null
  var re = /(I|i)ntroduced(;|,)\s*/

  if(isFinegrained){
    const distribution = select('(/bio:treatment/description[@type="distribution"])[1]//character[@name="distribution"]/@value', doc)

    if (!distribution || !distribution.length) {
      text = selectText('(bio:treatment/description[@type="distribution"])', doc) // check to see if distribution is actually parsed, if not, treat it like coarse grained files
    
      if (!text) {
        return false
      } else {
        values = text.replace(re, "").replace(/,/g, ";").split("; ")
      }

    } else {
      text = selectText('(/bio:treatment/description[@type="distribution"])[1]//text', doc)
      values = distribution.map(node => node.textContent.replace(re,"").replace(',United States', ''))
    }

  } else {
    text = selectText('(bio:treatment/description[@type="distribution"])', doc)
    if (!text) return false
    values = text.replace(re,"").replace(/,/g,";").split("; ")
  }
  return {
    text,
    values
  }
}
