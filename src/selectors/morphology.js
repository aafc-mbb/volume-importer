import {select, selectOne, selectText} from '../util/xpath'

const makeAttributeList = (attributes) => [...attributes].reduce((sum, attr) => Object.assign(sum, {[attr.name]: attr.value}), {})

export default (doc, isFinegrained) => {

  if (isFinegrained){
    const statements = select('/bio:treatment/description[@type="morphology"]/statement', doc).map(statement => {
      const entities = select('./biological_entity', statement).map(entity => {
        const characters = select('./character', entity)
        return Object.assign(
          makeAttributeList(entity.attributes),
          {
            characters: characters.map(character => makeAttributeList(character.attributes))
          }
        )
      })
      return {
        text: selectText('./text', statement),
        id: statement.getAttribute('id'),
        entities
      }
    })

    // const statements = select('/bio:treatment/description[@type="morphology"]//character', doc).map(character => {
    //   const statement = selectOne('ancestor::statement', character)
    //   const parent
    //   return {
    //     //biological_entity.name + character.name [+ constraint]
    //     name: selectText('parent::biological_entity/@name' character)
    //   }
    // })

    return statements

  } else {
    return [{text: selectText('/bio:treatment/description[@type="morphology"]',doc)}]
  }

}
