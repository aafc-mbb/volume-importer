import {select, selectOne, selectText} from '../util/xpath'

export default (doc) => {
  const basionyms = select('/bio:treatment/taxon_identification[@status="BASIONYM"]', doc);

  return basionyms.map(basionym => {
    const lastTaxonName = selectOne('taxon_name[last()]', basionym)
    const hierarchyText = selectText('taxon_hierarchy/text()', basionym)

    // grab the last item in the hierarchy, then get it's rank
    const hierarchyArr = hierarchyText
      .replace(/;$/, '') // remove any trailing semicolon
      .split(';')
      .map(rankAndName => rankAndName.split(' ').map(v => v.trim()))

    const hierarchyMap = hierarchyArr.reduce((obj, v) => (obj[v[0]] = v[1]) && obj, {})
    
    const authority = selectText('./@authority', lastTaxonName)

    const rank = hierarchyArr[hierarchyArr.length - 1][0]

    const publication_title = select('place_of_publication/publication_title/text()', basionym)
    const publication_place = select('place_of_publication/place_in_publication/text()', basionym)

    return {
      rank,
      authority: authority == 'unknown' ? '' : authority, // if the authority is unknown, display nothing
      publication_title: publication_title,
      publication_place: publication_place,
      hierarchy: hierarchyArr,
      hierarchyMap,
      hierarchyText: hierarchyText,
    }
  });

}
