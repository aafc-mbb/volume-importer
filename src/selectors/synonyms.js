import {select, selectOne, selectText} from 'util/xpath'
import log from 'util/log'

export default (doc, file) => {
  const synonyms = select('/bio:treatment/taxon_identification[@status="SYNONYM"]', doc);

  return synonyms.map(synonym => {
    const lastTaxonName = selectOne('taxon_name[last()]', synonym)
    const hierarchyText = selectText('taxon_hierarchy/text()', synonym)

    if (!hierarchyText) {
      log.xmlError('taxon_hierarchy missing from synonym', file)
      return false;
    }

    // grab the last item in the hierarchy, then get it's rank
    const hierarchyArr = hierarchyText && hierarchyText
      .replace(/;$/, '') // remove any trailing semicolon
      .split(';')
      .map(rankAndName => rankAndName.split(' ').map(v => v.trim())) || []

    const hierarchyMap = hierarchyArr.reduce((obj, v) => (obj[v[0]] = v[1]) && obj, {})
    
    const authority = selectText('./@authority', lastTaxonName)

    const rank = hierarchyArr[hierarchyArr.length - 1][0]

    return {
      rank,
      authority: authority == 'unknown' ? '' : authority, // if the authority is unknown, display nothing
      hierarchy: hierarchyArr,
      hierarchyMap,
      hierarchyText: hierarchyText,
    }
  }).filter(s => !!s);

}
