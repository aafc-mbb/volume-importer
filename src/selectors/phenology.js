import {select, selectOne, selectText} from '../util/xpath'

export default (doc, isFinegrained) => {
  var text = null
  var values = null
  if(isFinegrained){
    const phenology = selectOne('/bio:treatment/description[@type="phenology"]', doc)
    if (!phenology) {
      return false
    }
    text = selectText('.//text', phenology)
    values = select('.//character[@name="phenology"]', phenology).map(node => node.getAttribute('value'))
  } else {
      text = selectText('/bio:treatment/description[@type="phenology"]', doc)
      if(text == null) return false
  }
  return {
    text,
    values
  }
}
