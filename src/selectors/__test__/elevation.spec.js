import fs from 'fs'
import path from 'path'
import { expect } from 'chai'
import {DOMParser} from 'xmldom'
import selectElevation from '../elevation'
/*  
  (/bio:treatment/description[@type="elevation"])[1]//text
  (/bio:treatment/description[@type="elevation"])[1]//character[@name="elevation"]

  <description type="elevation">
    <statement id="elevation_0">
      <text>60–1300 m;</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" value="60–1300 m" />
      </biological_entity>
    </statement>
  </description>
*/


describe('elevation', function () {
  const acourtia = new DOMParser().parseFromString(fs.readFileSync(path.join(__dirname, '../../../test/fixtures/V19_10.xml'), 'utf8'))
  // const chaptaliaAlbicans = new DOMParser().parseFromString(fs.loadFileSync('fixtures/V19_20.xml'))
  
  it('should return an object with the original text and the parsed value', function () {
    const elevation = selectElevation(acourtia)
    expect(elevation).to.be.an('object')
    expect(elevation).to.have.all.keys(['text', 'value'])
    expect(elevation.text).to.equal('60–1300 m;')
    expect(elevation.value).to.equal('60–1300 m')
  })
})
