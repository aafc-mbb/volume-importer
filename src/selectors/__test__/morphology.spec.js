import fs from 'fs'
import path from 'path'
import { expect } from 'chai'
import {DOMParser} from 'xmldom'
import selectMorphology from '../morphology'

  /*
  /bio:treatment/description[@type="morphology"]
  */

const wrapFixture = fragment => `<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <description type="morphology">
    ${fragment}
  </description>
</bio:treatment>`
const simplePropertyFixture = wrapFixture(
  `<statement id="d0_s0">
    <text>Leaves sessile or nearly so;</text>
      <biological_entity id="o49873" name="leaf" name_original="leaves" type="structure">
        <character is_modifier="false" name="architecture" value="sessile" />
        <character name="architecture" value="nearly" />
      </biological_entity>
    </statement>`
)
const complexPropertyFixture = wrapFixture(
  `<statement id="d0_s1">
    <text>blades obovate to obovate-elliptic, 2–14 cm, margins retrorsely serrulate to denticulate-apiculate, abaxial faces white-tomentose, adaxial faces green, glabrous or glabrate.</text>
    <biological_entity id="o49874" name="blade" name_original="blades" type="structure">
      <character is_modifier="false" name="shape" notes="[duplicate value]" value="obovate" />
      <character is_modifier="false" name="shape" notes="[duplicate value]" value="obovate-elliptic" />
      <character char_type="range_value" from="obovate" name="shape" to="obovate-elliptic" />
      <character char_type="range_value" from="2" from_unit="cm" name="size" to="14" to_unit="cm" />
    </biological_entity>
    <biological_entity id="o49875" name="margin" name_original="margins" type="structure">
      <character is_modifier="false" modifier="retrorsely" name="architecture_or_shape" notes="[duplicate value]" value="serrulate" />
      <character is_modifier="false" name="architecture_or_shape" notes="[duplicate value]" value="denticulate-apiculate" />
      <character char_type="range_value" from="retrorsely serrulate" name="architecture_or_shape" to="denticulate-apiculate" />
    </biological_entity>
    <biological_entity constraint="abaxial" id="o49876" name="face" name_original="faces" type="structure">
      <character is_modifier="false" name="pubescence" value="white-tomentose" />
    </biological_entity>
  </statement>`
)
describe('morphology', function () {
  const simple = new DOMParser().parseFromString(simplePropertyFixture)
  const complex = new DOMParser().parseFromString(complexPropertyFixture)
  // const chaptaliaAlbicans = new DOMParser().parseFromString(fs.loadFileSync('fixtures/V19_20.xml'))

  it('should return an array of objects representing a flattened morphology schema', function () {
    const morphology = selectMorphology(simple)

    expect(morphology).to.be.an('array')
    expect(morphology).to.deep.equal([ { text: 'Leaves sessile or nearly so;',
      id: 'd0_s0',
      entities: [
        {
          id: 'o49873',
          name: 'leaf',
          name_original: 'leaves',
          type: 'structure',
          characters: [ 
            {is_modifier: 'false', name: 'architecture', value: 'sessile'},
            {name: 'architecture', value: 'nearly'}
          ]
        }
      ]
    }])
  })

  // it('should return an array of objects representing a flattened morphology schema', function () {
  //   const morphology = selectMorphology(simple)
  //   expect(morphology).to.be.an('array')
  //   expect(morphology).to.deep.equal([
  //     {
  //       text: 'blades obovate to obovate-elliptic, 2–14 cm, margins retrorsely serrulate to denticulate-apiculate, abaxial faces white-tomentose, adaxial faces green, glabrous or glabrate.',
  //       properties: [
  //         {
  //           name: 'blade shape',
  //           value: 'ovate'
  //         },
  //         {
  //           name: 'blade shape',
  //           value: 'ovate-elliptic'
  //         },
  //         {
  //           name: 'blade shape',
  //           range: ['']
  //         }
  //       ]
  //     }
  //   ])
  // })
})
