import fs from 'fs'
import path from 'path'
import { expect } from 'chai'
import {DOMParser} from 'xmldom'
import selectCommonName from '../common-name'

/*
  /bio:treatment/other_name[@type="common_name"]/text()

  <other_name type="common_name">Common cottonrose</other_name>
*/

describe('common-name', function () {
  const chaptalia = new DOMParser().parseFromString(fs.readFileSync(path.join(__dirname, '../../../test/fixtures/V19_20.xml'), 'utf8'))

  it('should return an array of common names', function () {
    const names = selectCommonName(chaptalia)
    expect(names).to.be.an('array')
    expect(names).to.deep.equal(['White sunbonnet'])
  })
})
