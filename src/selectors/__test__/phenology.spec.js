import fs from 'fs'
import path from 'path'
import { expect } from 'chai'
import {DOMParser} from 'xmldom'
import selectPhenology from '../phenology' 
  /*
  (/bio:treatment/description[@type="phenology"])[1]//text
  (/bio:treatment/description[@type="phenology"])[1]//character[@name=phenology] 

  //acourtia microcephala
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="phenology" value="may" />
        <character name="phenology" value="jul" />
        <character name="phenology" value="spring" />
        <character name="phenology" value="summer" />
      </biological_entity>
    </statement>
  </description>
  */

describe('phenology', function () {
  const acortia = new DOMParser().parseFromString(fs.readFileSync(path.join(__dirname, '../../../test/fixtures/V19_10.xml'), 'utf8'))
  // const chaptaliaAlbicans = new DOMParser().parseFromString(fs.loadFileSync('fixtures/V19_20.xml'))
  
  it('should return an object with the original text and an array of parsed values', function () {
    expect(selectPhenology(acortia)).to.be.an('object')
    
    expect(selectPhenology(acortia)).to.deep.equal({
      text: 'Flowering May–Jul.',
      values: ['may', 'jul', 'spring', 'summer']
    })  
  })

})
