import fs from 'fs'
import path from 'path'
import { expect } from 'chai'
import {DOMParser} from 'xmldom'
import selectHabitat from '../habitat'
/*
  (/bio:treatment/description[@type="habitat"])[1]//text
  (/bio:treatment/description[@type="habitat"])[1]//character[@name="habitat"]

  // Filago vulgaris
  <description type="habitat">
    <statement id="habitat_0">
      <text>Relatively dry, usually sandy soils, old fields, pastures, usually disturbed;</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="Relatively dry sandy soils" />
        <character name="habitat" value="old fields" />
        <character name="habitat" value="pastures" />
        <character name="habitat" value="disturbed ;" />
      </biological_entity>
    </statement>
  </description>
*/

describe('habitat', function () {
  const fixture = new DOMParser().parseFromString(fs.readFileSync(path.join(__dirname, '../../../test/fixtures/V19_742.xml'), 'utf8'))
  // const chaptaliaAlbicans = new DOMParser().parseFromString(fs.loadFileSync('fixtures/V19_20.xml'))
  
  it('should return an object with the original text and an array of parsed values', function () {
    const habitat = selectHabitat(fixture)
    expect(habitat).to.be.an('object')
    expect(habitat).to.have.all.keys(['text', 'values'])
    expect(habitat.text).to.equal('Relatively dry, usually sandy soils, old fields, pastures, usually disturbed;')
    expect(habitat.values).to.deep.equal([
      {
        modifier: null,
        value: 'Relatively dry sandy soils'
      },
      {
        modifier: null,
        value: 'old fields'
      },
      {
        modifier: null,
        value: 'pastures'
      },
      {
        modifier: null,
        value: 'disturbed ;'
      }
    ])
  })
})
