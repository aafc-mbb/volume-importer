import fs, { lchmod } from 'fs'
import path from 'path'
import { expect } from 'chai'
import {DOMParser} from 'xmldom'
import selectDistribution from '../distribution'
/*
  (/bio:treatment/description[@type="distribution"])[1]//text
  (/bio:treatment/description[@type="distribution"])[1]//character[@name="distribution"]

  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla.; Mexico; West Indies; Central America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Fla." />
        <character name="distribution" value="Mexico" />
        <character name="distribution" value="West Indies" />
        <character name="distribution" value="Central America." />
      </biological_entity>
    </statement>
  </description>
*/

describe('distribution', function () {
  const acortia = new DOMParser().parseFromString(fs.readFileSync(path.join(__dirname, '../../../test/fixtures/V19_20.xml'), 'utf8'))
  // const chaptaliaAlbicans = new DOMParser().parseFromString(fs.loadFileSync('fixtures/V19_20.xml'))
  
  it('should return an object with the original text and an array of parsed geographic locations', function () {
    const distribution = selectDistribution(acortia)
    expect(distribution).to.be.an('object')
    expect(distribution).to.have.all.keys('text', 'values')
    expect(distribution.text).to.equal('Fla.; Mexico; West Indies; Central America.')
    expect(distribution.values).to.deep.equal([
      'Fla.', 'Mexico', 'West Indies', 'Central America.'
    ])
  })
})
