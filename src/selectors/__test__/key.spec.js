import fs from 'fs'
import path from 'path'
import { expect } from 'chai'
import {DOMParser} from 'xmldom'
import selectKey from '../keys' 
  /*
  (/bio:treatment/key
  (/bio:treatment/key/
  */

describe('key', function () {
  const asteraceae = new DOMParser().parseFromString(fs.readFileSync(path.join(__dirname, '../../../test/fixtures/V19_1.xml'), 'utf8'))
  
  it('should return an array of key statements', function () {
    expect(selectKey(asteraceae)).to.be.an('array')
    
  })

})
