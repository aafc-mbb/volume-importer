import {expect} from 'chai'
import {DOMParser} from 'xmldom'
import selectAuthors from '../authors'
    //   /bio:treatment/meta/source/author/text()

const singleAuthorFragment = `<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
      <source>
         <author>David J. Keil</author>
         <date>unknown</date>
      </source>
      <processed_by>
         <processor>
            <date>2016/06/20 21:34:25</date>
            <software type="Semantic Markup" version="0.1.181-SNAPSHOT">CharaParser</software>
            <operator>Joel Sachs (jsachs@csee.umbc.edu) </operator>
            <resource type="OTO Glossary" version="0.19">Plant</resource>
         </processor>
      </processed_by>
  </meta>
</bio:treatment>`

const multipleAuthorFragment = `<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
      <source>
        <author>Theodore M. Barkley†,Luc Brouillet,John L. Strother</author>
        <date>unknown</date>
      </source>
      <processed_by>
         <processor>
            <date>2016/06/20 21:34:25</date>
            <software type="Semantic Markup" version="0.1.181-SNAPSHOT">CharaParser</software>
            <operator>Joel Sachs (jsachs@csee.umbc.edu) </operator>
            <resource type="OTO Glossary" version="0.19">Plant</resource>
         </processor>
      </processed_by>
  </meta>
</bio:treatment>`

const unknownAuthorFragment = `<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
      <source>
        <author>unknown</author>
        <date>unknown</date>
      </source>
      <processed_by>
         <processor>
            <date>2016/06/20 21:34:25</date>
            <software type="Semantic Markup" version="0.1.181-SNAPSHOT">CharaParser</software>
            <operator>Joel Sachs (jsachs@csee.umbc.edu) </operator>
            <resource type="OTO Glossary" version="0.19">Plant</resource>
         </processor>
      </processed_by>
  </meta>
</bio:treatment>`

describe('authors', function () {
  const singleAuthor = new DOMParser().parseFromString(singleAuthorFragment)
  const multiAuthor = new DOMParser().parseFromString(multipleAuthorFragment)
  const unknownAuthor = new DOMParser().parseFromString(unknownAuthorFragment)

  it('should return an array of author names even when only one author is listed', function () {
    const authors = selectAuthors(singleAuthor)
    expect(authors).to.be.an('array')
    expect(authors).to.have.lengthOf(1)
    expect(authors).to.deep.equal(['David J. Keil'])
  })

  it('should return an array of author names', function () {
    const authors = selectAuthors(multiAuthor)
    expect(authors).to.be.an('array')
    expect(authors).to.have.lengthOf(3)
    expect(authors).to.deep.equal(['Theodore M. Barkley†', 'Luc Brouillet', 'John L. Strother'])
  })

  it('should return an empty array if author is "unknown"', function () {
    const authors = selectAuthors(unknownAuthor)
    expect(authors).to.be.an('array')
    expect(authors).to.have.lengthOf(0)
  })
})
