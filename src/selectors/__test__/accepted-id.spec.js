import fs from 'fs'
import path from 'path'
import { expect } from 'chai'
import {DOMParser} from 'xmldom'
import selectAcceptedId from '../accepted-id'
/*
  /bio:treatment/taxon_identification[@status="ACCEPTED"]
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Martinov" date="unknown" rank="family">asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">mutisieae</taxon_name>
    <taxon_name authority="D. Don" date="1830" rank="genus">acourtia</taxon_name>
    <taxon_name authority="de Candolle in A. P. de Candolle and A. L. P. P. de Candolle" date="1838" rank="species">microcephala</taxon_name>
    <place_of_publication>
      <publication_title>in A. P. de Candolle and A. L. P. P. de Candolle, Prodr.</publication_title>
      <place_in_publication>7: 66. 1838</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe mutisieae;genus acourtia;species microcephala</taxon_hierarchy>
  </taxon_identification>
 */

const objRepresentation = {
  rank: 'species',
  authority: 'de Candolle in A. P. de Candolle and A. L. P. P. de Candolle',
  publications: [{
    title: 'in A. P. de Candolle and A. L. P. P. de Candolle, Prodr.',
    place: '7: 66. 1838'
  }],
  hierarchy: [
    ['family', 'asteraceae'],
    ['tribe', 'mutisieae'],
    ['genus', 'acourtia'],
    ['species', 'microcephala']
  ],
  hierarchyText: 'family asteraceae;tribe mutisieae;genus acourtia;species microcephala',
  hierarchyMap: {
    family: 'asteraceae',
    tribe: 'mutisieae',
    genus: 'acourtia',
    species: 'microcephala'
  },
  etymology: null
}

describe('acceptedId', function () {
  const doc = new DOMParser().parseFromString(fs.readFileSync(path.join(__dirname, '../../../test/fixtures/V19_10.xml'), 'utf8'))

  it('should return an object representation of the accepted ID', function () {
    const id = selectAcceptedId(doc)
    expect(id).to.be.an('object')
    expect(id).to.deep.equal(objRepresentation)
  })
})
