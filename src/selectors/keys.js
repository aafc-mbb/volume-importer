import {select, selectText} from '../util/xpath'
import ucFirst from '../util/ucfirst'

const textToId = (text) => (text && parseInt(text.trim().replace('.', ''))) || null

const findLinkWhenBrackets = (determination) => { //tested on Asteraceae brackets and Salix brackets 
  var link

  if (determination.includes(",")) { //if the determination includes two subtaxa
    link = determination.split(" \(")[0] //split on the bracket and take the first word
  } else {
    link = determination.match(/\((.*)\)/)[1] //if the determination includes a single subtaxon in brackets
  }
  return link
}

const getVerboseLink = (link, hierarchyMap) => {
  if (!link) {
    return null
  }
  if ((link.endsWith("eae") || link.endsWith("EAE")) && //if the taxon is a tribe, we need to output a verbose link to match tribe page title
    !link.endsWith("oideae") && !link.endsWith("OIDEAE")) {
    var link = ucFirst(hierarchyMap.family) + " tribe " + link
  }
  else if ((link.endsWith("inae") || link.endsWith("INAE")) && //if the taxon is a subtribe, we need to output a verbose link to match subtribe page title
    !link.endsWith("oideae") && !link.endsWith("OIDEAE") &&
    !link.endsWith("eae") && !link.endsWith("EAE")) {
    var link = ucFirst(hierarchyMap.family) + " (tribe " + hierarchyMap.tribe + ") subtribe " + link
  }
  return link
}

const reformatTerseGenusLink = (link, hierarchyMap) => { //some section and subgenera key links need reformatting, when they start with Sect. and Subg. with no genus prefix
  if (!link) {
    return null
  }
  if (link.startsWith("Sect.") || link.startsWith("Subg.")) {
    var link = ucFirst(hierarchyMap.genus) + " " + link.charAt(0).toLowerCase() + link.slice(1) // uppercase the genus but lowercase the words subg. and sect.
  }
  return link
}

const extractNameFromDetermination = (text, rank, hierarchyMap) => {
  if (text) {
    var determination
    var link

    const match = text.match(/^[\w]*.? ?([A-Z\u00C0-\u00DC]{1}[a-ÿ. -\u00E0-\u00FC]+)[ ,]*(\u0028{1}[,.;\w )]+\u0029{1})?.*/u)
    if (match) {
      determination = match[1]
      link = determination

      if ((determination.match(/^[\w]+ \([\w]+[, \w]{1,}\)/u) || determination.match(/^[\w. ]+\([\w. ]+ [, \w. ]{1,}\)/u)) && 
        determination.substr(-1) == ")") { //something appears in brackets, the determination ends in a bracket
        link = findLinkWhenBrackets(determination)
      }
      if (determination.includes("×")){ //don't get rid of special character in hybrid names
        determination = determination.replace(/\d+[a-z]? /, '')
        link = determination
      } 
    } else {
      determination = text.replace(/(\d+.)?\d+[a-z]? (\– \d+. )?/, '')
      link = determination
    }
  }
  
  if (determination && (rank == 'family' || rank == 'subfamily' || rank == 'tribe')) {
    link = getVerboseLink(link, hierarchyMap)
  } else if (link && (rank == 'genus' || rank == "subgenus")) { //replace exception cases with function call
    // console.log(reformatTerseGenusLink(link, hierarchyMap))
    link = reformatTerseGenusLink(link, hierarchyMap)
  }

  return {
    name: determination,
    link
  }
 }

const extractNextStepFromDetermination = (determinationText, type) => {
  if (!determinationText) {
    return null
  }
  if (type == 'Group') {
    var match = determinationText.match(/^Group (\d+)/)
  } else if (type == 'Key') {
    var match = determinationText.match(/^Key (\w+)/)
  } else if (type == 'Subkey') {
    var match = determinationText.match(/^Subkey (\w+)/)
  }
  if (match && match.length) {
    return match[1]
  }
  return null
}

export default (doc, rank, hierarchyMap) => {
  const keys = select('/bio:treatment/key', doc).map(key => {
      var head = selectText('./key_head', key);
      if(head == null) head = ``
      return {
      header: head,
      statements: select('./key_statement', key).map(statement => {
        let determinationText = selectText('./determination', statement)
        return {
          id: textToId(selectText('./statement_id', statement)),
          description: selectText('./description', statement),
          determination: extractNameFromDetermination(determinationText, rank, hierarchyMap),
          next: textToId(selectText('./next_statement_id', statement)),
          nextGroup: extractNextStepFromDetermination(determinationText, 'Group'),
          nextKey: extractNextStepFromDetermination(determinationText, 'Key'),
          nextSubkey: extractNextStepFromDetermination(determinationText, 'Subkey')
        }
      })
    }
  })
  return keys
}