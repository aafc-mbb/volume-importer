import {select, selectOne, selectText} from '../util/xpath'

export default (doc) => {
  const discussion = select(' /bio:treatment/discussion[not(@type="table")]/text()', doc) // exclude type="table"
  //console.log(discussion)
  if (!discussion || !discussion.length) {
    return false
  }
  return discussion.map(
    node => node.textContent.replace(/[\t\n\r ]+/g, ' ').trim()
    //console.log("node: " + node )
  )
}
