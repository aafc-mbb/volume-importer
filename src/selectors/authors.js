import {selectText} from '../util/xpath'
import authorAffiliations from '../constants/fna-affiliations'

const getAffiliationData = (affiliations) => {
  if (affiliations) {
    return ({
      volume: affiliations[3],
      institution: affiliations[1],
      location: affiliations[2]
    })
  } else {
    return false
  }
}

export default (doc) => {
  const authors = selectText(' /bio:treatment/meta/source/author', doc)

  if (!authors) {
    return false
  }

  const authorNames = authors === 'unknown' ? [] : authors.split(/,|;/)
  if (authorNames.length == 0) {
    return false
  }

  return authorNames.map(name => ({
      name: name.trim(),
      values: getAffiliationData(authorAffiliations.filter(p => p && p[0] === name.trim())[0]) // find the name in the affiliations list and return parsed data
    }))
  
}