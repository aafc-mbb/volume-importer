import {select, selectOne, selectText} from '../util/xpath'

export default (doc) => {
  const habitat = selectOne('/bio:treatment/description[@type="habitat"]', doc)
  if (!habitat) {
    return false
  }
  const text = selectText('.', habitat)
  const values = select('.//character[@name="habitat"]', habitat).map(node => ({
    modifier: null,
    value: node.getAttribute('value')
  }))
  return {
    text,
    values
  }
}
