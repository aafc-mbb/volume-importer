import {select, selectOne, selectText} from '../util/xpath'

export default (doc) => {
  const references = select('/bio:treatment/references/entry', doc)

  if (!references || !references.length) {
    return false
  }
  return references.map(node => ({
    id: node.getAttribute('id'),
    text: selectText('./*/verbatimtext', node)
  }))
}
