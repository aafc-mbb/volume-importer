import {select, selectOne, selectText} from '../util/xpath'

export default (doc) => {
  const treatmentPage = selectOne('/bio:treatment/meta/other_info_on_meta[@type="treatment_page"]', doc)

  if (!treatmentPage) {
    return false
  }
  const text = selectText('.', treatmentPage)
  
  return {
    text
  }
}

/*  
<meta>
  <source>
    <author>unknown</author>
    <date>unknown</date>
  </source>
  <other_info_on_meta type="volume">27</other_info_on_meta>
  <other_info_on_meta type="mention_page">189</other_info_on_meta>
  <other_info_on_meta type="treatment_page">194</other_info_on_meta>
</meta>
*/