import {selectText} from '../util/xpath'
import familyCopyrights from '../constants/family-copyrights'

// This runs on all taxon ranks, not just family. However, the lookup table only contains family-level data.

function returnCopyright(family) { // use the lookup table to find the copyright information for a family
    const copyright = familyCopyrights[family] ? familyCopyrights[family] : 'Flora of North America Association'
    // if it's not in the lookup table, let's assume it's FNAA
    return copyright
}

export default (doc, familyName) => {
    const illustratorDataExists = selectText('/bio:treatment/meta/other_info_on_meta[@type="illustrator"]', doc)
    // use illustrator data to figure out if there are plates that need a copyright notice

    if (!illustratorDataExists) {
        return false
    }
    return returnCopyright(familyName)
}
