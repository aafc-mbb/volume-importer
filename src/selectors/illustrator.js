import {select, selectOne, selectText} from '../util/xpath'

export default (doc) => {
  const illustrator =
  selectText('/bio:treatment/meta/other_info_on_meta[@type="illustrator"]', doc)
  //console.log("illustrator: " + illustrator)

  if (!illustrator) {
    return false
  }
  return illustrator.split(/, | and /)
}
