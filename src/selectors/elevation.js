import {select, selectOne, selectText} from '../util/xpath'

export default (doc) => {
  const elevation = selectOne('(/bio:treatment/description[@type="elevation"])', doc)
  if (!elevation) {
    return false
  }
  const text = selectText('.', elevation)
  const values = select('.//character[@name="elevation"]', elevation).map(node => ({
    modifier: null,
    value: node.getAttribute('value')
  }))
  return {
    text,
    values
  }
}