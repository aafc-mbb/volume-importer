import {select, selectOne, selectText} from '../util/xpath'

export default (doc) => {
  const tables = select(' /bio:treatment/discussion[@type="table"]/text()', doc)
  //console.log(tables)
  if (!tables || !tables.length) {
    return false
  }
  return tables.map(
    node => node.textContent.replace(/[\t\n\r ]+/g, ' ').trim()
    //console.log("node: " + node )
  )
}
