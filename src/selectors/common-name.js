import {select, selectOne, selectText} from '../util/xpath'

export default (doc) => {
  const names = select('/bio:treatment/other_name[@type="common_name"]/text()', doc)
  if (!names || !names.length) {
    return false
  }

  return names.map(
    node => node.data
  )
}
