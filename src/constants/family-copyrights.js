export default {
    'Malpighiaceae': 'University of Michigan Herbarium',
    'Vitaceae': 'Smithsonian Institution',
    'Araliaceae': 'Smithsonian Institution',
    'Poaceae': 'Utah State University'
}