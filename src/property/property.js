const formatPropertyName = (entity, character) => {
  if (character.hasAttribute('constraint')) {
    return `${entity.getAttribute('name')} ${character.getAttribute('name')} ${character.getAttribute('constraint')}`
  } else {
    return `${entity.getAttribute('name')} ${character.getAttribute('name')}`
  }
}


export function Property (character /*: Element*/, entity /*: Element*/) {

  const property = function () {
    return character.hasAttribute('char_type') 
      ? [character.getAttribute('from'), character.getAttribute('to')]
      : character.getAttribute('value')
  }
  property.type = character.hasAttribute('char_type')
    ? 'range'
    : 'value'

  property.getFlatName = () => formatPropertyName(entity, character)

  return property
}