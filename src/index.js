import mediawikiTransform from './transform/mediawiki'
import log from 'util/log'
const program = require('commander')
const colors = require('colors/safe')
const fs = require('fs-extra')
const path = require('path')
const simpleGit = require('simple-git/promise') 

program
.version('0.1.0')
.arguments('<dir>')
program.parse(process.argv)

main(program)

// get all XML files from directory
async function main (program) {
  try {
    // get working path from first cli argument
    const dirPath = path.resolve(path.normalize(program.args[0]))
    var repoInfo = null;
    // check if the given path of XML files is a GIT repo
    // if it is, add links to the processed files back to the original
    // files in the repo for debugging
    const git = simpleGit(dirPath);
    const isRepo = await git.checkIsRepo()
    if (isRepo) {
      const remote = await git.raw(['config', '--get', 'remote.origin.url'])
      if (remote) {
        console.log(colors.green(`Git repo detected with remote ${remote}`))
        // get path of given directory relative to git root
        const relativeRepoPath = (await git.raw(['rev-parse', '--show-prefix'])).trim()
        // get hash of current HEAD
        const commitHash = (await git.raw(['rev-parse', 'HEAD'])).trim();
        // convert git@ url to publicly accessible url
        // this should work with github, gitlab and bitbucket
        const repoHttpBase = remote.trim().replace(/git@([\w]+\.[\w]+):(.+)\.git/i, 'https://$1/$2')
        const repoHttpCommitBase = `${repoHttpBase}/src/${commitHash}/${relativeRepoPath}`;

        console.log(colors.green(`Repo URL is ${repoHttpBase} at HEAD ${commitHash}`))

        repoInfo = {
          relativePath: relativeRepoPath, 
          baseUrl: repoHttpCommitBase,
          remote,
        }
        // console.dir(repoInfo);
      }
    }
    if (!repoInfo) {
      console.log(colors.red('No git repo detected or no remote repo info found. No links to original XML files will be attached to processed treatments.'))
    }

    const dirContents = await fs.readdir(dirPath)
    const files = dirContents.filter(filename => path.extname(filename).toLowerCase() === '.xml')
    console.log(`Transforming ${files.length} files`)
    await mediawikiTransform(program, {files, dirPath, repoInfo})
    console.log("\n");
    log.dump();
    console.log("\nDone.")
  } catch (e) {
    console.error(e)
  }
}
