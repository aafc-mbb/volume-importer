import { fromPairs } from "ramda";

import wrapPage from "./helpers/wrap-page";
import wrapExport from "./helpers/wrap-export";
import template from "./helpers/template";
import parserFunction from "./helpers/parser-function";
import formatName from "../../formatters/name";
import nsIds from "../../constants/namespace-ids";

import transformTitle from "./transformers/title";
import transformMorphologyStatement from "./transformers/morphology-statements";
import transformKeysToHtml from "./transformers/keys";
import transformIdPublication from "./transformers/id-publication";
import transformBasionyms from "./transformers/basionym";
import transformSynonyms from "./transformers/synonym";
import transformHierarchyToNav from "./transformers/hierarchy-nav";

import transformSpecialStatus, {
  codeToName
} from "./transformers/special-status";
import { processCategories, getCategoryPages } from "./category";

// import transformHierarchy from './transformers/hierarchy'

export default function(
  {
    acceptedId,
    authors,
    basionyms,
    commonNames,
    specialStatus,
    discussion,
    tables,
    distribution,
    introduced,
    elevation,
    habitat,
    illustrator,
    illustrationCopyright,
    keys,
    morphology,
    phenology,
    references,
    synonyms,
    volume,
    mentionPages,
    treatmentPage
  },
  { name, familyName, morphologyProperties },
  { urlToGit }
  ) {
  const namespaceId = nsIds.MAIN;
  const title = transformTitle(name);
  const formattedHierarchy = acceptedId.hierarchy.map(([rank, name]) => [
    rank,
    formatName(acceptedId, rank)
  ]);

  const treatmentIdTemplate = template("Treatment/ID", {
    accepted_name: name,
    accepted_authority: acceptedId.authority,
    // 'volume_number': 19,
    publications: acceptedId.publications.map(transformIdPublication).join(", "),
    common_names: commonNames && commonNames.join(";"),
    special_status:
      specialStatus && specialStatus.map(transformSpecialStatus).join(""),
    basionyms: transformBasionyms(basionyms),
    synonyms: transformSynonyms(synonyms),
    hierarchy: formattedHierarchy.map(([rank, name]) => name).join(";"),
    hierarchy_nav: transformHierarchyToNav(formattedHierarchy),
    etymology: acceptedId.etymology,
    volume: `Volume ${volume.text}`,
    mention_page: mentionPages ? `page ${mentionPages}` : "",
    treatment_page: treatmentPage.text ? `page ${treatmentPage.text}` : ""
  });

  const statementHtml = transformMorphologyStatement(
    morphology,
    morphologyProperties
  );

  const treatmentReferences =
    references && references.map(ref => template("Treatment/Reference", ref));

  const treatmentBodyTemplate = template("Treatment/Body", {
    phenology: phenology && phenology.text,
    habitat: habitat && habitat.text,
    elevation: elevation && elevation.text,
    distribution: distribution && distribution.values.join(";"),
    introduced: !!introduced,
    discussion:
      discussion && discussion.map(d => `<p>${d}</p>`).join("<!--\n-->"),
    tables: tables ? tables.map(t => `{{${name} ${t}}}`).join("") : "",
    references: treatmentReferences ? treatmentReferences.join("") : ""
  });

  const propertyAssignments = Object.assign(
    {
      name,
      author: authors && authors.map(author => author.name).join(";"),
      authority: acceptedId.authority,
      rank: acceptedId.rank,
      "parent rank":
        formattedHierarchy.length <= 2
          ? formattedHierarchy.length > 1
            ? "family"
            : ""
          : formattedHierarchy[formattedHierarchy.length - 2][0],
      synonyms: synonyms.map(id => formatName(id)).join(";"),
      basionyms: basionyms.map(id => formatName(id)).join(";"),
      family: familyName,
      phenology: phenology && phenology.text,
      habitat: habitat && habitat.text,
      illustrator: illustrator && illustrator.join(";"),
      "illustration copyright": illustrationCopyright && illustrationCopyright,
      elevation: elevation && elevation.text,
      distribution: distribution && distribution.values.join(";"),
      introduced: !!introduced,
      reference: references ? references.map(ref => ref.id).join(";") : "None",
      "publication title": acceptedId.publications.map(p => p.title).join(";"),
      "publication year": acceptedId.publications.map(p => p.year).join(";"),
      "special status": specialStatus && specialStatus.map(codeToName).join(";") || '',
      "source xml": urlToGit || ''
    },
    // output a variable assignment for each parent rank
    fromPairs(formattedHierarchy),
    // build assignments from morphology properties
    fromPairs(
      Object.entries(
        morphologyProperties.reduce((obj, property) => {
          let value;
          let propertyName = property.name;
          if (property.type === "range") {
            // propertyName = propertyName.concat(' range')
            value = `${property.value[0][0]}${property.value[0][1] || ""};${
              property.value[1][0]
            }${property.value[1][1] || ""}`;
          } else {
            if (property.modifier && property.modifier === "not") {
              value = `not ${property.value}`;
            } else {
              value = property.value;
            }
          }
          // this overwrites range values with the last declared value
          // that might not be desirable, but we'll need to come up with a way to separate multiple range
          // values to fix it
          if (value) {
            obj[propertyName] =
              obj[propertyName] === undefined || property.type === "range"
                ? value
                : value.concat(`;${obj[propertyName]}`);
          }
          return obj;
        }, {})
      ).sort((a, b) => (a[0] < b[0] ? -1 : 1))
    )
  );

  const keyHtml = transformKeysToHtml(keys);

  const taxonFunction = parserFunction("Taxon", propertyAssignments);
  const category =
    formattedHierarchy.length > 1 &&
    formattedHierarchy[formattedHierarchy.length - 2][1];
  const parentCategory =
    formattedHierarchy.length > 2 &&
    formattedHierarchy[formattedHierarchy.length - 3][1];

  const categoryAssignments = ["Treatment", category]
    .filter(c => !!c)
    .map(c => `[[Category:${c}]]`)
    .join("");

  const res = {
    title,
    namespaceId,
    text: [
      treatmentIdTemplate,
      statementHtml,
      treatmentBodyTemplate,
      keyHtml,
      taxonFunction,
      categoryAssignments
    ].join("<!--\n\n-->")
  };

  var cat =
    category && parentCategory
      ? category + ";" + parentCategory
      : category
      ? category
      : undefined;

  if (cat) {
    processCategories(cat);
  }

  return wrapPage(res);
}
