import wrapPage from './helpers/wrap-page'
import defineTemplate from './helpers/define-template'
import nsIds from '../../constants/namespace-ids'
import propertyCategories from '../../constants/superproperty-descriptor'
import superProperties from '../../constants/superproperty-structure'
const properties = new Map()

const relationshipPropertyNames = {
  typeof: 'Type of',
  partof: 'Part of',
}

export function processProperties ({}, {name, morphologyProperties, familyName}) {
  morphologyProperties.forEach(({
    type,
    name,
    structureName,
    structureConstraint,
    characterName,
    rangeFromProperty,
    rangeToProperty
  }) => {
    properties.has(name) || properties.set(name, {
      type,
      name,
      structureName,
      structureConstraint,
      characterName,
      rangeFromProperty,
      rangeToProperty
    })
  })

  superProperties.forEach(([structure, hasParent, parent, relationship, description]) => {
    properties.has(structure) || properties.set(structure, {
      type: null,
      name: structure,
      structureName: structure,
      structureConstraint: null,
      characterName: null
    })
  })
}

export function getPropertyPages () {
  return [...properties.values()].sort((a, b) => a.name < b.name ? -1 : 0).map(property => {
    return wrapPage({
      title: property.name,
      namespaceId: nsIds.PROPERTY,
      text: generatePropertyPage(property)
    })
  })
}

function generatePropertyPage ({
    type,
    name,
    characterName,
    structureName,
    structureConstraint,
    rangeFromProperty,
    rangeToProperty,
    isTopLevel
  }) {
  const pageText = []
  if (type === 'range') {
    pageText.push(`* A [[Has type::Record]] property defining a range value. It defined the following fields: [[Has fields::${rangeFromProperty}; ${rangeToProperty}]]`)
  } else {
    pageText.push(`* A [[Has type::Text]] property defining a single value.`)
  }
  if (isTopLevel) {
    pageText.push
  }
  pageText.push(`* Structure: [[Subproperty of::${structureName}]]`)
  if (structureConstraint) {
    pageText.push(`* It is a [[Subproperty of::${structureConstraint}]]`)
    pageText.push(`* It is a property of structure constraint: [[Subproperty of::${structureConstraint}]]`)
  }
  if (characterName) {
    pageText.push(`* Character: [[Subproperty of::${characterName}]]`)
  }
  // find any matching entries in propertyCategories and add category to the property page
  propertyCategories.filter(p => p && p[0] === characterName).forEach(p => p[1] === 1 && pageText.push(
    `* A [[:Category:${p[2]}|${p[2]}]] property [[Category:${p[2]}]]`
  ))
  // filter for entries in the superProperties array which match this structure name
  superProperties.filter(p => p && p[0] === structureName).forEach(p => {
    const [structure, hasParent, parent, relationship, description] = p;
    // naive assumption that
    if (hasParent === 1) {
      const relationshipProperty = relationshipPropertyNames[relationship] || null; 
      pageText.push(`* A subproperty of [[Subproperty of::${parent}]]`);
      if (relationshipProperty) {
        pageText.push(`* A [[Property:${relationshipProperty}|${relationshipProperty}]] [[${relationshipProperty}::Property:${parent}|${parent}]]`)
      }
    } else {
      pageText.push(`* A [[:Category:Top Level Property|Top Level Property]] [[Category:Top Level Property]]`);
    }
  })

  pageText.push(`==Types of ${name}==\n{{#ask: [[${relationshipPropertyNames['typeof']}::Property:${name}]]|limit=500|format=category|default=None.}}`);
  pageText.push(`==Parts of ${name}==\n{{#ask: [[${relationshipPropertyNames['partof']}::Property:${name}]]|limit=500|format=category|default=None.}}`);
  
  return pageText.join('\n')
}