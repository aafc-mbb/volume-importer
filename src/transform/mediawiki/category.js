import wrapPage from './helpers/wrap-page'
import nsIds from '../../constants/namespace-ids'
import familyCategories from './helpers/family-categories'
import ucFirst from '../../util/ucfirst'
//import transformTitle from './transformers/title'

const allCategories = new Set()
//const keptCategories = new Set()

export function processCategories (cat){
  cat && allCategories.add(cat)
}

export function getCategoryPages () {
  return [...allCategories].map(category => {
    return wrapPage({
      title: category.replace(/;.*/,''),
      namespaceId: nsIds.CATEGORY,
      text: generateCategoryPage({category})
    })
  })
}

function aboveFamilyCategories (name) { // store an object of asserted category taxonomy from FNA titling
  const parentCategory = familyCategories[name] // verify family names against this hierarchy to add appropriate parent category
  return parentCategory
}

function generateCategoryPage ({
    category
  }) {
    var match = category.match(/^(.*);(.*)/u)
    const categoryName = match ? match[1] : category
    const parentCategoryText = match ? `${categoryName} is a subcategory of [[:Category:${match[2]}|${match[2]}]][[Category:${match[2]}]].` : null

    const aboveFamily = aboveFamilyCategories(categoryName.toLowerCase())
    const aboveFamilyCategoryText = aboveFamily ? `${categoryName} is a subcategory of [[:Category:${ucFirst(aboveFamily)}|${ucFirst(aboveFamily)}]][[Category:${ucFirst(aboveFamily)}]].` : ``

    const pageText = []
    pageText.push(`This is the category page for ${categoryName}.  For the treatment page see [[${categoryName}]].`)
    pageText.push(parentCategoryText)
    pageText.push(aboveFamilyCategoryText)
    return pageText.join('\n')
}
