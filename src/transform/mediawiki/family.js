import wrapPage from './helpers/wrap-page'
import defineTemplate from './helpers/define-template'
import nsIds from '../../constants/namespace-ids'

const fixedTreatmentProperties = [
  {type: 'value', name: 'author'},
  {type: 'value', name: 'authority'},
  {type: 'value', name: 'common name'},
  {type: 'value', name: 'synonyms'},
  {type: 'value', name: 'basionyms'},
  {type: 'value', name: 'elevation'},
  {type: 'value', name: 'distribution'},
  {type: 'value', name: 'introduced'},
  {type: 'value', name: 'habitat'},
  {type: 'value', name: 'illustrator'},
  {type: 'value', name: 'illustration copyright'},
  {type: 'value', name: 'phenology'},
  {type: 'value', name: 'place in publication'},
  {type: 'value', name: 'publication title'},
  {type: 'value', name: 'publication year'},
  {type: 'value', name: 'reference'},
  {type: 'value', name: 'special status'},
  {type: 'value', name: 'source xml'}
]

export function processFamily ({}, {
  name, 
  morphologyProperties, 
  familyName
}) {
  const family = Family.getFamily(familyName)
  // console.log(morphologyProperties)
  family.addProperties(morphologyProperties)
}

export function getFamilyTemplatePages () {
  return Family.toPages()
}

function outputPropertyAssignation ({type, name}) {
  if (type === 'range') {
    return `{{#if: {{{${name}|}}} | {{#set: ${name}={{{${name}}}} }} }}`    
  } else {
    return `{{#if: {{{${name}|}}} | {{#arraymap:{{{${name}}}}|;|@@@|{{#set: ${name}=@@@ }}|&#32; }} }}`
  }
}

function Family (name) {
  this.name = name
  this.properties = new Map()
  Family.families.push(name)
}
Family.families = []
Family.isDefined = function (name) {
  return Family.families.indexOf(name) > -1
}
Family.getFamily = function (name) {
  if (!Family.isDefined(name) ) {
    Family[name] = new Family(name)
  }
  // console.log(`Family ${name}`, Family[name])
  return Family[name]
}
Family.map = function (fn) {
  return Family.families.map(family => fn(Family.getFamily(family)))
}
Family.toPages = function () {
  return Family.map(family => wrapPage({
    title: family.name,
    namespaceId: nsIds.TEMPLATE,
    text: family.toTemplate()
  }))
}
Family.prototype.toTemplate = function () {
  const sortedProperties = [...this.properties.values()]
    .concat(fixedTreatmentProperties)
    .sort((a, b) => a.name < b.name ? -1 : 1)
  const documentation = `This template is used to set the semantic properties for treatments of the ${this.name} family
  It defines the following properties:
${sortedProperties.map(({name}) => `* [[Property:${name}]]`).join("\n")}`

  return defineTemplate({
    documentation,
    page: sortedProperties
      .map(outputPropertyAssignation)
      .join("<!--\n-->")
      .concat('__SHOWFACTBOX__')
  })
}
Family.prototype.addProperties = function (properties) {
  properties.forEach(({
    type,
    name,
    structureName,
    structureConstraint,
    characterName
  }) => {
    this.properties.has(name) || this.properties.set(name, {
      type,
      name,
      structureName,
      structureConstraint,
      characterName
    })
  })
}
