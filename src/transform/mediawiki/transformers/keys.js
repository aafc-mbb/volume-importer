const baseClass = 'treatment-key'

const transformKeyToHtml = function (key, num) {
  return `<div class="${baseClass}-group">
  ${key.header && `<h3 class="${baseClass}-header" id="key-${num}">${key.header}</h3>`}
{| class="wikitable fna-keytable"
${key.statements.map(st => ([
  // new row with elementId of the statement (linked from description col)
  `|-id=key-${num}-${st.id}`,
  // first col, statement id
  `|${st.id}`,
  // second col, description
  `|${st.description || ''}`,
  // third col, next group, id or determination
  `|${formatDetermination(key, num, st)}`
]
.join('\n'))
).join('\n')}
|}
</div>`
}

export default function (keys) {
  return keys.length ? `<div class="treatment-key">
==${keys.length > 1 ? 'Keys' : 'Key'}==
${keys.map(transformKeyToHtml).join('<!--\n-->')}</div>` : ''
}

function formatDetermination (key, num, st) {
  if (st.nextGroup) {
    return `[[#key-${st.nextGroup}|Group ${st.nextGroup}]]`
  }
  if (st.nextKey) {
    return `[[#key-${st.nextKey.charCodeAt(0) - 64}|Key ${st.nextKey}]]` // Translate letter into number using char codes. Next groups are called next Keys
  }
  if (st.nextSubkey) {
    return `[[#key-${st.nextSubkey.charCodeAt(0) - 64}|Subkey ${st.nextSubkey}]]` // Next groups are called next Subkeys
  }
  if (st.next) {
    return `[[#key-${num}-${st.next}| > ${st.next}]]`
  }
  if (st.determination) {
    return st.determination.link ? `[[${st.determination.link}|${st.determination.name}]]` : `${st.determination.name}`
  }
}