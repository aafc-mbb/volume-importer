export default (hierarchy) => {
  return `<div class="higher-taxa">`
    .concat(hierarchy.map(([rank, name]) => `<div class="higher-taxon"><small>${rank}</small>[[${name}]]</div>`).join(''))
    .concat(`</div>`);
}