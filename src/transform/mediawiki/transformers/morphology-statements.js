// [ { text: 'Leaves sessile or nearly so;',
// id: 'd0_s0',
// entities: [
//   {
//     attributes: {
//       id: 'o49873',
//       name: 'leaf',
//       name_original: 'leaves',
//       type: 'structure'
//     },
//     characters: [
//       {is_modifier: 'false', name: 'architecture', value: 'sessile'},
//       {name: 'architecture', value: 'nearly'}
//     ]
//   }
// ]
// }]

const boldText = function (statementText) {
  //if(!statementText==null)
  return statementText.replace(/^([A-Z][\w]+[\s:,\.-]{1})/g, '<b>$1</b>') // bold first word
  .replace(/((?<!ser|sect|subg|subfam)\.\s)([A-Z]{1}[a-z]+)/g, '$1<b>$2</b>') // bold subsequent words
  .replace(/((?<!ser|sect|subg|subfam)\.\s)(x)/g, '$1<b>$2</b>').replace(/(\.\s)(\dn)/g, '$1<b>$2</b>') // bold x and n
}

export default function (statements, allProperties) {
  return statements.map(function (statement) {
    if(statement.text == null){
      return null
    }
    const statementProperties = allProperties.filter(function (property) {
      return property.statementId === statement.id;
    }).map(function (property) {
      return property.name;
    });
    // Bold all words starting with an uppercase letter or at the start of a sentence
    return `<span class="statement" id="st-${statement.id}" data-properties="${statementProperties.join(';')}">${boldText(statement.text)}</span>`
  }).join(' ')
}
