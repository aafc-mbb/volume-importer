import template from '../helpers/template'

export default ({title /*:String*/, place/*:String*/, year/*:String*/, other_info_on_pub/*:String*/}) => template('Treatment/Publication', {
  title,
  place,
  year,
  other_info_on_pub
})