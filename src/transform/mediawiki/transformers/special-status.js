import template from '../helpers/template'

const codesToNames = {
  C: 'Conservation concern',
  E: 'Endemic',
  F: 'Illustrated',
  W: 'Weedy',
  I: 'Introduced',
}
export default (code) => template('Treatment/ID/Special_status', {
  code: code,
  label: codesToNames[code] ? codesToNames[code] : ''
});

export const codeToName = (code) => codesToNames[code] ? codesToNames[code] : code;