import { expect } from 'chai'
import transformIdPublication from '../id-publication.js'

// output of acceptedId selector
const fixture = {
  rank: 'species',
  publication: [{
    title: 'in A. P. de Candolle and A. L. P. P. de Candolle, Prodr.',
    place: '7: 66. 1838'
  }],
  hierarchyMap: {
    family: 'asteraceae',
    tribe: 'mutisieae',
    genus: 'acourtia',
    species: 'microcephala'
  }
}

const expectedOutput = `{{Treatment/Publication
|title=in A. P. de Candolle and A. L. P. P. de Candolle, Prodr.
|place=7: 66. 1838
}}`

describe('id publication', function () {
  it('should return a correctly formatted publication template string', function () {
    const output = transformIdPublication(fixture.publication[0])
    expect(output).to.be.a('string')
    expect(output).to.equal(expectedOutput)
  })

  it('should correctly format names for all ranks')
})
