import formatName from '../../../formatters/name'
import template from '../helpers/template'

export default (ids) => {
    return ids.map(id => template('Treatment/ID/Basionym', { name: formatName(id), authority: id.authority, rank: id.rank, publication_title: id.publication_title, publication_place: id.publication_place})).join(' ')
}