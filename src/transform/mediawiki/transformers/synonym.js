import formatName from '../../../formatters/name'
import template from '../helpers/template'

export default (ids) => {
    return ids.map(id => template('Treatment/ID/Synonym', { name: formatName(id), authority: id.authority, rank: id.rank })).join(' ')
}