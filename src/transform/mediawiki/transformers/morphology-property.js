/**
 * Transforms the ouput of the morphology selector to an array of objects with
 * a property name and value and some meta-data of the original statement
 */

const formatPropertyName = function ({entity, character}) {
  if (entity.constraint) {
    return `${entity.constraint} ${entity.name} ${character.name.replace(/_/g, ' ')}`
  } else {
    return `${entity.name} ${character.name.replace(/_/g, ' ')}`
  }
}

export default function (statements, isFinegrained) {
  // return statements.map(
  //   statement => statement.entities.map(
  //     entity => entity.characters.map(
  //       transformEntityCharacterToProperty({statement, entity, character})
  //     ).reduce((flat, arr) => flat.concat(arr), [])))

  if(isFinegrained){
    const properties = statements.reduce((list, statement) => {
      return statement.entities.reduce(
        (list, entity) => list.concat(entity.characters.map(
          character => transformEntityCharacterToProperty({statement, entity, character})
        )
      ),
      list)
    }, [])
    return properties

  } //else {
    const properties = []
    properties.push({text: statements})
    return properties
//}

}

function transformEntityCharacterToProperty ({statement, entity, character}) {
  const property = {
    type: character.from && character.to ? 'range' : 'value',
    name: formatPropertyName({entity, character}),
    modifier: character.modifier || null,
    statementId: statement.id,
    characterName: character.name,
    structureName: entity.name,
    structureConstraint: entity.constraint || null
  }
  if (property.type === 'value') {
    property.value = character.value
  }
  if (property.type === 'range') {
    const rangePropertyPrefix = getRangePropertyName(character);
    property.rangeFromProperty = `${rangePropertyPrefix} from`;
    property.rangeToProperty = `${rangePropertyPrefix} to`;
    property.value = [[character.from, character.from_unit], [character.to, character.to_unit]]
  }
  return property
}

function getRangePropertyName (character) {
  let property;

  switch (character.to_unit) {
    case 'm':
    case 'cm':
    case 'mm':
      property = `Dimensional measurement`;
      break;
    default:
      property = 'Unknown range';
      break;
  }

  return property;
}
