export default function (name, obj) {
  return `{{${name}
|${Object.entries(obj)
  .filter(([k, v]) => v !== false && v !== null && v !== undefined)
  .map(arr => arr.join('=')).join("\n|")}
}}`
}
