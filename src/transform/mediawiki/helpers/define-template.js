export default function ({documentation, page}) {
  return `<noinclude>${documentation}</noinclude><!--
--><includeonly>${page}</includeonly>`
}