let pageId = 1
import escape from 'escape-html'
export default ({title, namespaceId, text}) => `
<page>
  <title>${title}</title>
  <ns>${namespaceId}</ns>
  <id>${pageId++}</id>
  <revision>
    <contributor>
      <username>Volume Importer</username>
      <id>1</id>
    </contributor>
    <model>wikitext</model>
    <format>text/x-wiki</format>
    <text xml:space="preserve"><![CDATA[${text}]]></text>
  </revision>
</page>
`
