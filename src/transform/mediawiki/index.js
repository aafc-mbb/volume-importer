import { DOMParser } from "xmldom";
import { selectOne } from "util/xpath";
import log from 'util/log'

import treatment from "./treatment";
import { processFamily, getFamilyTemplatePages } from "./family";
import { processProperties, getPropertyPages } from "./property";
import { processAuthors, getAuthorPages } from "./author";
import { processCategories, getCategoryPages } from "./category";
import wrapExport from "./helpers/wrap-export";

import selectAcceptedId from "selectors/accepted-id";
import selectAuthors from "selectors/authors";
import selectBasionyms from "selectors/basionyms";
import selectCommonNames from "selectors/common-name";
import selectSpecialStatus from "selectors/special-status";
import selectDiscussion from "selectors/discussion";
import selectTables from "selectors/tables";
import selectDistribution from "selectors/distribution";
import selectIntroduced from "selectors/introduced";
import selectElevation from "selectors/elevation";
import selectHabitat from "selectors/habitat";
import selectIllustrator from "selectors/illustrator";
import selectIllustrationCopyright from "selectors/illustration-copyright";
import selectKeys from "selectors/keys";
import selectMorphology from "selectors/morphology";
import selectPhenology from "selectors/phenology";
import selectReferences from "selectors/references";
import selectSynonyms from "selectors/synonyms";
import selectVolume from "selectors/volume";
import selectMentionPages from "selectors/mention-page";
import selectTreatmentPage from "selectors/treatment-page";

import formatName from "formatters/name";

import morphologyToProperties from "./transformers/morphology-property";

const cliProgress = require('cli-progress')
const fs = require("fs-extra");
const path = require("path");

// get all XML files from directory
export default async function(program, { dirPath, files, repoInfo }) {
  try {
    const outputFilename = program.args[1] || `fna-${Date.now()}.xml`;
    const outputPath = path.join(
      path.resolve(path.join(__dirname, "../../../")),
      "output"
    );
    const outputDir = await fs.ensureDir(outputPath);

    // create a new progress bar instance and use shades_classic theme
    const progressBar = new cliProgress.Bar({
      stopOnComplete: true,
      clearOnComplete: true,
      etaBuffer: Math.round(files.length / 10),
      fps: 4,
    }, cliProgress.Presets.rect);
    
    // start the progress bar with a total value of 200 and start value of 0
    progressBar.start(files.length, 0);

    // process all files
    const transforming = files
      .map(filename => path.join(dirPath, filename))
      .map(async (filePath, i) => {
        const file = await processFile(filePath, repoInfo, progressBar)
        progressBar.increment();
        return file
      });
      
    return Promise.all(transforming).then(async function(treatmentPages) {
      // const outputFilepath = path.join(await outputDir.then(() => outputPath), outputFilename)
      const familyTemplates = getFamilyTemplatePages();
      const propertyPages = getPropertyPages();
      const authorPages = getAuthorPages();
      const categoryPages = getCategoryPages();
      // const exportXML = wrapExport({
      //   pages: [].concat(familyTemplates).concat(propertyPages).concat(pages)
      // })
      console.log("\nWriting files...");
      return Promise.all([
        fs.writeFile(
          path.join(outputPath, "families.xml"),
          wrapExport({
            pages: familyTemplates
          })
        ),
        fs.writeFile(
          path.join(outputPath, "properties.xml"),
          wrapExport({
            pages: propertyPages
          })
        ),
        fs.writeFile(
          path.join(outputPath, "authors.xml"),
          wrapExport({
            pages: authorPages
          })
        ),
        fs.writeFile(
          path.join(outputPath, "treatments.xml"),
          wrapExport({
            pages: treatmentPages
          })
        ),
        fs.writeFile(
          path.join(outputPath, "categories.xml"),
          wrapExport({
            pages: categoryPages
          })
        )
      ]).then((res) => {
        console.log(
          `Wrote ${
            familyTemplates.length
          } family templates to output/family.xml`
        );
        console.log(
          `Wrote ${
            propertyPages.length
          } property definition pages to output/properties.xml`
        );
        console.log(
          `Wrote ${
            treatmentPages.length
          } treament pages to ouput/treatments.xml`
        );
        console.log(
          `Wrote ${categoryPages.length} category pages to ouput/categories.xml`
        );
        return res
      }).catch(() => {
        log.error('Failed to write XML files')
      });

    });
  } catch (e) {
    console.error(e);
  }
}
const processFile = async function(filePath, repoInfo) {
  const xml = await fs.readFile(filePath, "utf8");
  const fileName = path.basename(filePath);
  // process.stdout.write(`.`);
  // console.log(`Transforming ${fileName}`);
  var urlToGit = null;

  // if processing from a git repo, set the full URL to this file
  if (repoInfo) {
    urlToGit = `${repoInfo.baseUrl}${fileName}`;
  }

  const doc = new DOMParser().parseFromString(xml, "utf8");
  const isFinegrained =
    selectOne("/bio:treatment/meta/processed_by", doc) == null ? false : true;

  const acceptedId = selectAcceptedId(doc, fileName);
  const authors = selectAuthors(doc, fileName);
  const basionyms = selectBasionyms(doc, fileName);
  const commonNames = selectCommonNames(doc, fileName);
  const specialStatus = selectSpecialStatus(doc, fileName);
  const discussion = selectDiscussion(doc, fileName);
  const tables = selectTables(doc, fileName);
  const distribution = selectDistribution(doc, isFinegrained);
  const introduced = selectIntroduced(doc, isFinegrained);
  const elevation = selectElevation(doc, fileName);
  const habitat = selectHabitat(doc, fileName);
  const illustrator = selectIllustrator(doc, fileName);
  const keys = selectKeys(doc, acceptedId.rank, acceptedId.hierarchyMap);
  const morphology = selectMorphology(doc, isFinegrained);
  const phenology = selectPhenology(doc, isFinegrained);
  const references = selectReferences(doc, fileName);
  const synonyms = selectSynonyms(doc, fileName);
  const volume = selectVolume(doc, fileName);
  const mentionPages = selectMentionPages(doc, fileName);
  const treatmentPage = selectTreatmentPage(doc, fileName);

  const name = formatName(acceptedId);

  const familyName = formatName(acceptedId, "family");

  const illustrationCopyright = selectIllustrationCopyright(doc, familyName, fileName);

  const selected = {
    acceptedId,
    authors,
    basionyms,
    commonNames,
    specialStatus,
    discussion,
    tables,
    distribution,
    introduced,
    elevation,
    habitat,
    illustrator,
    illustrationCopyright,
    keys,
    morphology,
    phenology,
    references,
    synonyms,
    volume,
    mentionPages,
    treatmentPage
  };
  const computed = {
    name,
    familyName,
    morphologyProperties: morphologyToProperties(morphology, isFinegrained)
  };

  // process this treatment to build family template
  processFamily(selected, computed);
  // process this treatment to build property pages
  processProperties(selected, computed);
  // process this treatment to build author pages
  processAuthors(selected, computed);
  // process this category to build author pages
  //  processCategories(selected, computed)

  // process this treatment and return a MediaWiki <page> export
  return Promise.resolve(treatment(selected, computed, { urlToGit }));
}
