import wrapPage from './helpers/wrap-page'
import nsIds from '../../constants/namespace-ids'
const allAuthors = new Set()
const allAuthorNames = new Set()

// now the transformer will receive authors as an array with name = ; values = volume:, institution:, location:

export function processAuthors ({
  authors
}) {

  authors && authors.map(author => {
    const authorName = author.name // This is a super hacky way of getting around the fact that sets don't work on objects
    if (!allAuthorNames.has(authorName)) { // see https://stackoverflow.com/questions/41404607/why-does-javascript-set-not-do-unique-objects?noredirect=1&lq=1 for example
      allAuthorNames.add(authorName)// add to name checking list
      allAuthors.add(author) // add to author data
    }
  }) // if(!allAuthors.has(author)) allAuthors.add(author)
  //console.log(allAuthors)
}

export function getAuthorPages () {
  return [...allAuthors].map(author => {
    return wrapPage({
      title: author.name,
      namespaceId: nsIds.AUTHOR,
      text: generateAuthorPage({author}) // insert all the author data into this function
    })
  })
}

function generateAuthorPage ({
    author
  }) {
    const pageText = []
    pageText.push(`{{Author`)
    if (author.values) {
      pageText.push(`|affiliation_volume=${author.values.volume}`)
      pageText.push(`|institution=${author.values.institution}`)
      pageText.push(`|location=${author.values.location}`)
      }
    pageText.push(`|author=${author.name}}}`)
    return pageText.join('\n')
}

/*
{{Author
|author=Donald J. Pinkava}}

*/
