<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
      <source>
         <author>unknown</author>
         <date>unknown</date>
      </source>
      <processed_by>
         <processor>
            <date>2016/06/20 21:34:25</date>
            <software type="Semantic Markup" version="0.1.181-SNAPSHOT">CharaParser</software>
            <operator>Joel Sachs (jsachs@csee.umbc.edu) </operator>
            <resource type="OTO Glossary" version="0.19">Plant</resource>
         </processor>
      </processed_by>
  </meta>
  
  
  
  
  
  
  
  
  
  
  <taxon_identification status="ACCEPTED">
      <taxon_name authority="Martinov" date="unknown" rank="family">asteraceae</taxon_name>
      <taxon_name authority="Cassini" date="unknown" rank="tribe">cardueae</taxon_name>
      <taxon_name authority="Miller" date="1754" rank="genus">cirsium</taxon_name>
      <taxon_name authority="(Linnaeus) Scopoli" date="1772" rank="species">arvense</taxon_name>
      <place_of_publication>
         <publication_title>Fl. Carniol. ed.</publication_title>
         <place_in_publication>2, 2: 126. 1772</place_in_publication>
      </place_of_publication>
      <taxon_hierarchy>family asteraceae;tribe cardueae;genus cirsium;species arvense</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
      <statement id="d0_s0">
         <text>Perennials, dioecious or nearly so, 30–120 (–200) cm;</text>
      </statement>
      <statement id="d0_s1">
         <text>colonial from deep-seated creeping roots producing adventitious-buds.</text>
         <biological_entity id="o76773" name="perennial" name_original="perennials" type="structure">
            <character is_modifier="false" name="reproduction" value="dioecious" />
            <character name="reproduction" value="nearly" />
            <character char_type="range_value" from="120" from_inclusive="false" from_unit="cm" name="atypical_size" to="200" to_unit="cm" />
            <character char_type="range_value" from="30" from_unit="cm" name="size" to="120" to_unit="cm" />
            <character constraint="from roots" constraintid="o76774" is_modifier="false" name="growth_form" value="colonial" />
         </biological_entity>
         <biological_entity id="o76774" name="root" name_original="roots" type="structure">
            <character is_modifier="true" name="location" value="deep-seated" />
            <character is_modifier="true" name="growth_form_or_orientation" value="creeping" />
         </biological_entity>
         <biological_entity id="o76775" name="adventitious-bud" name_original="adventitious-buds" type="structure" />
         <relation from="o76774" id="r6512" name="producing" negation="false" to="o76775" />
      </statement>
      <statement id="d0_s2">
         <text>Stems 1–many, erect, glabrous to appressed gray-tomentose;</text>
         <biological_entity id="o76776" name="stem" name_original="stems" type="structure">
            <character char_type="range_value" from="1" is_modifier="false" name="count" to="many" />
            <character is_modifier="false" name="orientation" value="erect" />
            <character is_modifier="false" name="pubescence" notes="[duplicate value]" value="glabrous" />
            <character is_modifier="false" name="pubescence" notes="[duplicate value]" value="appressed" />
            <character is_modifier="false" name="pubescence" notes="[duplicate value]" value="gray-tomentose" />
            <character char_type="range_value" from="glabrous" name="pubescence" to="appressed gray-tomentose" />
         </biological_entity>
      </statement>
      <statement id="d0_s3">
         <text>branches 0–many, ascending.</text>
         <biological_entity id="o76777" name="branch" name_original="branches" type="structure">
            <character char_type="range_value" from="0" is_modifier="false" name="count" to="many" />
            <character is_modifier="false" name="orientation" value="ascending" />
         </biological_entity>
      </statement>
      <statement id="d0_s4">
         <text>Leaves: blades oblong to elliptic, 3–30 × 1–6 cm, margins plane to revolute, entire and spinulose, dentate, or shallowly to deeply pinnatifid, lobes well separated, lance-oblong to triangular-ovate, spinulose to few-toothed or few-lobed near base, main spines 1–7 mm, abaxial faces glabrous to densely gray-tomentose, adaxial green, glabrous to thinly tomentose;</text>
         <biological_entity id="o76778" name="leaf" name_original="leaves" type="structure" />
         <biological_entity id="o76779" name="blade" name_original="blades" type="structure">
            <character is_modifier="false" name="shape" notes="[duplicate value]" value="oblong" />
            <character is_modifier="false" name="shape" notes="[duplicate value]" value="elliptic" />
            <character char_type="range_value" from="oblong" name="shape" to="elliptic" />
            <character char_type="range_value" from="3" from_unit="cm" name="length" to="30" to_unit="cm" />
            <character char_type="range_value" from="1" from_unit="cm" name="width" to="6" to_unit="cm" />
         </biological_entity>
         <biological_entity id="o76780" name="margin" name_original="margins" type="structure">
            <character is_modifier="false" name="shape" notes="[duplicate value]" value="plane" />
            <character is_modifier="false" name="shape" notes="[duplicate value]" value="revolute" />
            <character char_type="range_value" from="plane" name="shape" to="revolute" />
            <character is_modifier="false" name="architecture_or_shape" value="entire" />
            <character is_modifier="false" name="architecture_or_shape" value="spinulose" />
            <character is_modifier="false" name="shape" notes="[duplicate value]" value="dentate" />
            <character is_modifier="false" modifier="shallowly deeply" name="shape" notes="[duplicate value]" value="pinnatifid" />
            <character char_type="range_value" from="dentate or" name="shape" to="shallowly deeply pinnatifid" />
            <character is_modifier="false" name="shape" notes="[duplicate value]" value="dentate" />
            <character is_modifier="false" modifier="shallowly deeply" name="shape" notes="[duplicate value]" value="pinnatifid" />
            <character char_type="range_value" from="dentate or" name="shape" to="shallowly deeply pinnatifid" />
         </biological_entity>
         <biological_entity id="o76781" name="lobe" name_original="lobes" type="structure">
            <character is_modifier="false" modifier="well" name="arrangement" value="separated" />
            <character is_modifier="false" name="shape" notes="[duplicate value]" value="lance-oblong" />
            <character is_modifier="false" name="shape" notes="[duplicate value]" value="triangular-ovate" />
            <character is_modifier="false" name="shape" notes="[duplicate value]" value="spinulose" />
            <character is_modifier="false" name="shape" notes="[duplicate value]" value="few-toothed" />
            <character is_modifier="false" name="shape" notes="[duplicate value]" value="few-lobed" />
            <character char_type="range_value" from="lance-oblong" name="shape" to="triangular-ovate spinulose" />
            <character is_modifier="false" name="shape" notes="[duplicate value]" value="lance-oblong" />
            <character is_modifier="false" name="shape" notes="[duplicate value]" value="triangular-ovate" />
            <character is_modifier="false" name="shape" notes="[duplicate value]" value="spinulose" />
            <character is_modifier="false" name="shape" notes="[duplicate value]" value="few-toothed" />
            <character is_modifier="false" name="shape" notes="[duplicate value]" value="few-lobed" />
            <character char_type="range_value" constraint="near base" constraintid="o76782" from="lance-oblong" name="shape" to="triangular-ovate spinulose" />
         </biological_entity>
         <biological_entity id="o76782" name="base" name_original="base" type="structure" />
         <biological_entity constraint="main" id="o76783" name="spine" name_original="spines" type="structure">
            <character char_type="range_value" from="1" from_unit="mm" name="size" to="7" to_unit="mm" />
         </biological_entity>
         <biological_entity constraint="abaxial" id="o76784" name="face" name_original="faces" type="structure">
            <character is_modifier="false" name="pubescence" notes="[duplicate value]" value="glabrous" />
            <character is_modifier="false" modifier="densely" name="pubescence" notes="[duplicate value]" value="gray-tomentose" />
            <character char_type="range_value" from="glabrous" name="pubescence" to="densely gray-tomentose" />
         </biological_entity>
         <biological_entity constraint="adaxial" id="o76785" name="face" name_original="faces" type="structure">
            <character is_modifier="false" name="coloration" value="green" />
            <character is_modifier="false" name="pubescence" notes="[duplicate value]" value="glabrous" />
            <character is_modifier="false" modifier="thinly" name="pubescence" notes="[duplicate value]" value="tomentose" />
            <character char_type="range_value" from="glabrous" name="pubescence" to="thinly tomentose" />
         </biological_entity>
      </statement>
      <statement id="d0_s5">
         <text>basal absent at flowering, petioles narrowly winged, bases tapered;</text>
         <biological_entity id="o76786" name="leaf" name_original="leaves" type="structure" />
         <biological_entity constraint="basal" id="o76787" name="leaf" name_original="leaves" type="structure">
            <character constraint="at flowering" is_modifier="false" name="count" value="absent" />
         </biological_entity>
         <biological_entity id="o76788" name="petiole" name_original="petioles" type="structure">
            <character is_modifier="false" modifier="narrowly" name="architecture" value="winged" />
         </biological_entity>
         <biological_entity id="o76789" name="base" name_original="bases" type="structure">
            <character is_modifier="false" name="shape" value="tapered" />
         </biological_entity>
      </statement>
      <statement id="d0_s6">
         <text>principal larger cauline proximally winged-petiolate, distally sessile, well distributed, gradually reduced, not decurrent;</text>
         <biological_entity id="o76790" name="leaf" name_original="leaves" type="structure" />
         <biological_entity constraint="principal larger cauline" id="o76791" name="leaf" name_original="leaves" type="structure">
            <character is_modifier="false" modifier="proximally" name="architecture" value="winged-petiolate" />
            <character is_modifier="false" modifier="distally" name="architecture" value="sessile" />
            <character is_modifier="false" modifier="well" name="arrangement" value="distributed" />
            <character is_modifier="false" modifier="gradually" name="size" value="reduced" />
            <character is_modifier="false" modifier="not" name="shape" value="decurrent" />
         </biological_entity>
      </statement>
      <statement id="d0_s7">
         <text>distal cauline becoming bractlike, entire, toothed, or lobed, spinulose or not.</text>
         <biological_entity id="o76792" name="leaf" name_original="leaves" type="structure" />
         <biological_entity constraint="distal cauline" id="o76793" name="leaf" name_original="leaves" type="structure">
            <character is_modifier="false" name="shape" value="bractlike" />
            <character is_modifier="false" name="shape" value="entire" />
            <character is_modifier="false" name="shape" value="toothed" />
            <character is_modifier="false" name="shape" value="lobed" />
            <character is_modifier="false" name="shape" value="toothed" />
            <character is_modifier="false" name="shape" value="lobed" />
            <character is_modifier="false" name="architecture_or_shape" value="spinulose" />
            <character name="architecture_or_shape" value="not" />
         </biological_entity>
      </statement>
      <statement id="d0_s8">
         <text>Heads 1–many, borne singly or in corymbiform or paniculiform arrays at tips of main-stem and branches.</text>
         <biological_entity id="o76794" name="head" name_original="heads" type="structure">
            <character char_type="range_value" from="1" is_modifier="false" name="count" to="many" />
            <character constraint="in arrays" constraintid="o76795" is_modifier="false" name="arrangement" value="singly" />
            <character is_modifier="false" name="arrangement" value="in corymbiform or paniculiform arrays" />
         </biological_entity>
         <biological_entity id="o76795" name="array" name_original="arrays" type="structure">
            <character is_modifier="true" name="architecture" value="corymbiform" />
            <character is_modifier="true" name="architecture" value="paniculiform" />
         </biological_entity>
         <biological_entity id="o76796" name="tip" name_original="tips" type="structure" />
         <biological_entity id="o76797" name="main-stem" name_original="main-stem" type="structure" />
         <biological_entity id="o76798" name="branch" name_original="branches" type="structure" />
         <relation from="o76795" id="r6513" name="at" negation="false" to="o76796" />
         <relation from="o76796" id="r6514" name="part_of" negation="false" to="o76797" />
         <relation from="o76796" id="r6515" name="part_of" negation="false" to="o76798" />
      </statement>
      <statement id="d0_s9">
         <text>Peduncles 0.2–7 cm.</text>
         <biological_entity id="o76799" name="peduncle" name_original="peduncles" type="structure">
            <character char_type="range_value" from="0.2" from_unit="cm" name="size" to="7" to_unit="cm" />
         </biological_entity>
      </statement>
      <statement id="d0_s10">
         <text>Involucres ovoid in flower, ± campanulate in fruit, 1–2 × 1–2 cm, arachnoid tomentose, ± glabrate.</text>
         <biological_entity id="o76800" name="involucre" name_original="involucres" type="structure">
            <character constraint="in flower" constraintid="o76801" is_modifier="false" name="shape" value="ovoid" />
            <character constraint="in fruit" constraintid="o76802" is_modifier="false" modifier="more or less" name="shape" notes="alterIDs:o76801" value="campanulate" />
            <character char_type="range_value" from="1" from_unit="cm" name="length" notes="alterIDs:o76802" to="2" to_unit="cm" />
            <character char_type="range_value" from="1" from_unit="cm" name="width" notes="alterIDs:o76802" to="2" to_unit="cm" />
            <character is_modifier="false" name="pubescence" value="arachnoid" />
            <character is_modifier="false" name="pubescence" value="tomentose" />
            <character is_modifier="false" modifier="more or less" name="pubescence" value="glabrate" />
         </biological_entity>
         <biological_entity id="o76801" name="flower" name_original="flower" type="structure" />
         <biological_entity id="o76802" name="fruit" name_original="fruit" type="structure" />
      </statement>
      <statement id="d0_s11">
         <text>Phyllaries in 6–8 series, strongly imbricate, (usually purple-tinged), ovate (outer) to linear (inner), abaxial faces with narrow glutinous ridge, outer and middle appressed, entire, apices ascending to spreading, spines 0–1 mm (fine);</text>
         <biological_entity id="o76803" name="phyllarie" name_original="phyllaries" type="structure">
            <character is_modifier="false" modifier="strongly" name="arrangement" notes="alterIDs:o76804" value="imbricate" />
            <character is_modifier="false" name="shape" notes="[duplicate value]" value="ovate" />
            <character is_modifier="false" name="shape" notes="[duplicate value]" value="linear" />
            <character char_type="range_value" from="ovate" name="shape" to="linear" />
         </biological_entity>
         <biological_entity id="o76804" name="series" name_original="series" type="structure">
            <character char_type="range_value" from="6" is_modifier="true" name="count" to="8" />
         </biological_entity>
         <biological_entity constraint="abaxial" id="o76805" name="face" name_original="faces" type="structure">
            <character is_modifier="false" name="position" notes="alterIDs:o76806" value="outer" />
            <character is_modifier="false" name="position" value="middle" />
            <character is_modifier="false" name="fixation_or_orientation" value="appressed" />
            <character is_modifier="false" name="architecture_or_shape" value="entire" />
         </biological_entity>
         <biological_entity id="o76806" name="ridge" name_original="ridge" type="structure">
            <character is_modifier="true" name="size_or_width" value="narrow" />
            <character is_modifier="true" name="coating" value="glutinous" />
         </biological_entity>
         <biological_entity id="o76807" name="apex" name_original="apices" type="structure">
            <character is_modifier="false" name="orientation" notes="[duplicate value]" value="ascending" />
            <character is_modifier="false" name="orientation" notes="[duplicate value]" value="spreading" />
            <character char_type="range_value" from="ascending" name="orientation" to="spreading" />
         </biological_entity>
         <biological_entity id="o76808" name="spine" name_original="spines" type="structure">
            <character char_type="range_value" from="0" from_unit="mm" name="size" to="1" to_unit="mm" />
         </biological_entity>
         <relation from="o76803" id="r6516" name="in" negation="false" to="o76804" />
         <relation from="o76805" id="r6517" name="with" negation="false" to="o76806" />
      </statement>
      <statement id="d0_s12">
         <text>apices of inner phyllaries flat, ± flexuous, margins entire to minutely erose or ciliolate.</text>
         <biological_entity id="o76809" name="apex" name_original="apices" type="structure">
            <character is_modifier="false" name="prominence_or_shape" value="flat" />
            <character is_modifier="false" modifier="more or less" name="course" value="flexuous" />
         </biological_entity>
         <biological_entity constraint="inner" id="o76810" name="phyllarie" name_original="phyllaries" type="structure" />
         <biological_entity id="o76811" name="margin" name_original="margins" type="structure">
            <character is_modifier="false" name="architecture" notes="[duplicate value]" value="entire" />
            <character is_modifier="false" modifier="minutely" name="architecture" notes="[duplicate value]" value="erose" />
            <character char_type="range_value" from="entire" name="architecture" to="minutely erose" />
            <character is_modifier="false" name="pubescence" value="ciliolate" />
         </biological_entity>
         <relation from="o76809" id="r6518" name="part_of" negation="false" to="o76810" />
      </statement>
      <statement id="d0_s13">
         <text>Corollas purple (white or pink);</text>
      </statement>
      <statement id="d0_s14">
         <text>staminate 12–18 mm, (remaining longer than pappus when head fully mature), tubes 8–11 mm, throats 1–1.5 mm, lobes 3–5 mm;</text>
         <biological_entity id="o76812" name="corolla" name_original="corollas" type="structure">
            <character is_modifier="false" name="coloration_or_density" value="purple" />
            <character is_modifier="false" name="architecture" value="staminate" />
         </biological_entity>
         <biological_entity id="o76813" name="whole_organism" name_original="" type="structure">
            <character char_type="range_value" from="12" from_unit="mm" name="size" to="18" to_unit="mm" />
         </biological_entity>
         <biological_entity id="o76814" name="tube" name_original="tubes" type="structure">
            <character char_type="range_value" from="8" from_unit="mm" name="size" to="11" to_unit="mm" />
         </biological_entity>
         <biological_entity id="o76815" name="throat" name_original="throats" type="structure">
            <character char_type="range_value" from="1" from_unit="mm" name="size" to="1.5" to_unit="mm" />
         </biological_entity>
      </statement>
      <statement id="d0_s15">
         <text>pistillate 14–20 mm, (overtopped by pappi in fruit), tubes 10–15 mm, throats ca. 1 mm, lobes 2–3 mm;</text>
         <biological_entity id="o76816" name="lobe" name_original="lobes" type="structure">
            <character char_type="range_value" from="3" from_unit="mm" name="size" to="5" to_unit="mm" />
            <character is_modifier="false" name="architecture" value="pistillate" />
         </biological_entity>
         <biological_entity id="o76817" name="whole_organism" name_original="" type="structure">
            <character char_type="range_value" from="14" from_unit="mm" name="size" to="20" to_unit="mm" />
         </biological_entity>
         <biological_entity id="o76818" name="tube" name_original="tubes" type="structure">
            <character char_type="range_value" from="10" from_unit="mm" name="size" to="15" to_unit="mm" />
         </biological_entity>
         <biological_entity id="o76819" name="throat" name_original="throats" type="structure">
            <character name="size" unit="mm" value="1" />
         </biological_entity>
         <biological_entity id="o76820" name="lobe" name_original="lobes" type="structure">
            <character char_type="range_value" from="2" from_unit="mm" name="size" to="3" to_unit="mm" />
         </biological_entity>
      </statement>
      <statement id="d0_s16">
         <text>style tips 1–2 mm.</text>
         <biological_entity constraint="style" id="o76821" name="tip" name_original="tips" type="structure">
            <character char_type="range_value" from="1" from_unit="mm" name="size" to="2" to_unit="mm" />
         </biological_entity>
      </statement>
      <statement id="d0_s17">
         <text>Cypselae brown, 2–4 mm, apical collar not differentiated;</text>
         <biological_entity id="o76822" name="cypsela" name_original="cypselae" type="structure">
            <character is_modifier="false" name="coloration" value="brown" />
            <character char_type="range_value" from="2" from_unit="mm" name="size" to="4" to_unit="mm" />
         </biological_entity>
         <biological_entity constraint="apical" id="o76823" name="collar" name_original="collar" type="structure">
            <character is_modifier="false" modifier="not" name="variability" value="differentiated" />
         </biological_entity>
      </statement>
      <statement id="d0_s18">
         <text>pappi 13–32 mm, exceeding corollas.</text>
         <biological_entity id="o76825" name="corolla" name_original="corollas" type="structure" />
         <relation from="o76824" id="r6519" name="exceeding" negation="false" to="o76825" />
      </statement>
      <statement id="d0_s19">
         <text>2n = 34.</text>
         <biological_entity id="o76824" name="pappus" name_original="pappi" type="structure">
            <character char_type="range_value" from="13" from_unit="mm" name="size" to="32" to_unit="mm" />
         </biological_entity>
         <biological_entity constraint="2n" id="o76826" name="chromosome" name_original="" type="structure">
            <character name="count" value="34" />
         </biological_entity>
      </statement>
   </description>
  <description type="phenology"><statement id="phenology_0"><text>Flowering summer (Jun–Oct).</text><biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure"><character name="phenology" value="summer" /><character name="phenology" value="jun" /><character name="phenology" value="oct" /><character name="phenology" value="jul" /><character name="phenology" value="aug" /><character name="phenology" value="fall" /></biological_entity></statement></description>
  <description type="habitat"><statement id="habitat_0"><text>Disturbed sites, fields, pastures, roadsides, forest openings;</text><biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure"><character name="habitat" value="Disturbed sites" /><character name="habitat" value="fields" /><character name="habitat" value="pastures" /><character name="habitat" value="roadsides" /><character name="habitat" value="forest openings ;" /></biological_entity></statement></description>
  <description type="elevation"><statement id="elevation_0"><text>0–2600 m;</text><biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure"><character name="elevation" value="0–2600 m" /></biological_entity></statement></description>
  <description type="distribution"><statement id="distribution_0"><text>introduced; Greenland; St. Pierre and Miquelon; Alta., B.C., Man., N.B., Nfld. and Labr. (Nfld.), N.W.T., N.S., Ont., P.E.I., Que., Sask., Yukon; Ala., Alaska, Ariz., Ark., Calif., Colo., Conn., Del., D.C., Idaho, Ill., Ind., Iowa, Kans., Ky., Maine, Md., Mass., Mich., Minn., Mo., Mont., Nebr., Nev., N.H., N.J., N.Mex., N.Y., N.C., N.Dak., Ohio, Oreg., Pa., R.I., S.Dak., Tenn., Tex., Utah, Vt., Va., Wash., W.Va., Wis., Wyo.; native, Eurasia.</text><biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure"><character name="distribution" value="introduced" /><character name="distribution" value="Greenland" /><character name="distribution" value="St. Pierre and Miquelon" /><character name="distribution" value="Alta." /><character name="distribution" value="B.C." /><character name="distribution" value="Man." /><character name="distribution" value="N.B." /><character name="distribution" value="Nfld. and Labr. (Nfld.)" /><character name="distribution" value="N.W.T." /><character name="distribution" value="N.S." /><character name="distribution" value="Ont." /><character name="distribution" value="P.E.I." /><character name="distribution" value="Que." /><character name="distribution" value="Sask." /><character name="distribution" value="Yukon" /><character name="distribution" value="Ala." /><character name="distribution" value="Alaska" /><character name="distribution" value="Ariz." /><character name="distribution" value="Ark." /><character name="distribution" value="Calif." /><character name="distribution" value="Colo." /><character name="distribution" value="Conn." /><character name="distribution" value="Del." /><character name="distribution" value="D.C." /><character name="distribution" value="Idaho" /><character name="distribution" value="Ill." /><character name="distribution" value="Ind." /><character name="distribution" value="Iowa" /><character name="distribution" value="Kans." /><character name="distribution" value="Ky." /><character name="distribution" value="Maine" /><character name="distribution" value="Md." /><character name="distribution" value="Mass." /><character name="distribution" value="Mich." /><character name="distribution" value="Minn." /><character name="distribution" value="Mo." /><character name="distribution" value="Mont." /><character name="distribution" value="Nebr." /><character name="distribution" value="Nev." /><character name="distribution" value="N.H." /><character name="distribution" value="N.J." /><character name="distribution" value="N.Mex." /><character name="distribution" value="N.Y." /><character name="distribution" value="N.C." /><character name="distribution" value="N.Dak." /><character name="distribution" value="Ohio" /><character name="distribution" value="Oreg." /><character name="distribution" value="Pa." /><character name="distribution" value="R.I." /><character name="distribution" value="S.Dak." /><character name="distribution" value="Tenn." /><character name="distribution" value="Tex." /><character name="distribution" value="Utah" /><character name="distribution" value="Vt." /><character name="distribution" value="Va." /><character name="distribution" value="Wash." /><character name="distribution" value="W.Va." /><character name="distribution" value="Wis." /><character name="distribution" value="Wyo." /><character name="distribution" value="native" /><character name="distribution" value="Eurasia." /></biological_entity></statement></description>
  <number>2.</number>
  <other_name type="common_name">Canada or creeping or field thistle</other_name>
  <other_name type="common_name">chardon du Canada ou des champs</other_name>
  <other_name type="common_name">cirse des champs</other_name>
  <discussion>Cirsium arvense is one of the most economically important agricultural weeds in the world. It was introduced to North America in the 1600s and soon was recognized as a problem weed. Weed control legislation against the species was passed by the Vermont legislature in 1795 (R. J. Moore 1975). Canada thistle is now listed as a noxious weed in most areas where it occurs. It has very high seed production, and the runner roots readily survive the fragmentation that accompanies cultivation.</discussion>
  <discussion>Numerous variants of Cirsium arvense have been named based upon such features as pubescence, extent of leaf division, and spininess. Although extreme variants can be strikingly different, they are connected by such a web of intermediates that there seems to be little value in according any of them formal taxonomic recognition.</discussion>
    <references>
  <entry id="moore1975a">
  <referenceid>7cfc0e751fabf54812daef58003966e0</referenceid>
  <article>
    <verbatimtext>Moore, R. J. 1975. The biology of Canadian weeds. 13. Cirsium arvense (L.) Scop. Canad. J. Bot. 55: 1033–1048.</verbatimtext>
    <author>
      <person>
        <first>R.J.</first>
        <last>Moore</last>
      </person>
    </author>
    <year>1975</year>
    <title>The biology of Canadian weeds. 13. Cirsium arvense (L.) Scop</title>
    <journal>Canad. J. Bot</journal>
    <volume>55</volume>
    <pages>1033–1048</pages>
    <language/>
  </article>
</entry>
</references>
</bio:treatment>